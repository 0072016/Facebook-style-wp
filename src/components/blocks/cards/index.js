module.exports = {
  EventCard: require('./eventCard/EventCard'),
  GameCard: require('./gameCard/GameCard'),
  GameCardAdd: require('./gameCardAdd/GameCardAdd'),
  MoreCard: require('./moreCard/MoreCard'),
  VideoCard: require('./videoCard/VideoCard')
};
